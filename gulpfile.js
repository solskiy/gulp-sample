// gulpfile.js

// Include gulp
var gulp = require('gulp');
// Requires the gulp-sass plugin to compile scss
var sass = require('gulp-sass');
// Requires the browser-sync plugin
var browserSync = require('browser-sync').create();
// Requires the gulp-useref plugin - to concat js and css files
var useref = require('gulp-useref');

// Requires to minify
var uglify = require('gulp-uglifyjs');
var gulpIf = require('gulp-if');

// Requires the gulp-cssnano plugin - to minify css file
var cssnano = require('gulp-cssnano');
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

// gulp.task('test', function() {
//  console.log('console - test');
// });

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
})

gulp.task('servejs', function() {
  gulp.src('node_modules/jquery/dist/jquery.min.js')
  .pipe(gulp.dest('app/js'))
});

//compile bootstrap js

gulp.task('bjs', function(){
  gulp.src('node_modules/bootstrap/js/dist/*.js')
  	.pipe(uglify('bootstrap.js', {
  		mangle: true,
  		beatify:true
  	}
	))
  	.pipe(gulp.dest('app/js'))
});

gulp.task('serve',['bjs','servejs'])

gulp.task('sass', function(){
  return gulp.src(['node_modules/bootstrap/scss/**/*.scss','app/scss/**/*.scss'])
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('build', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify({
      mangle: true,
      compress: {
		sequences: true,
		dead_code: true,
		conditionals: true,
		booleans: true,
		// unused: true,
		if_return: true,
		join_vars: true,
		drop_console: true
	 }
  		})))
    // Minifies only if it's a CSS file
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

gulp.task('default', ['serve','browserSync'], function(){

  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);

  // Other watchers
})
